CREATE VIEW management AS
SELECT Department.Dnumber,Department.Dname,Manager.Fname,Manager.Lname
FROM Department JOIN Manager
ON Department.Dnumber = Manager.MNumber;
