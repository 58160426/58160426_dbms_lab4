CREATE VIEW empdept AS
SELECT Employee.Fname,Employee.Lname,Employee.Salary,Department.Dname,Dept_Locations.LocationName
FROM  Department JOIN Dept_Locations ON Department.Dnumber = Dept_Locations.Lnumber JOIN Employee ON Department.Dnumber = Employee.EmpID;

